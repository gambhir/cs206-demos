name := "cs206-demos"
version := "0.1.2"
scalaVersion := "3.2.2"

val akkaVersion = "2.8.2"
val logbackVersion = "1.2.11"

libraryDependencies ++= Seq(
  ("com.storm-enroute" %% "scalameter-core" % "0.21").cross(CrossVersion.for3Use2_13),
  "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.4",
  "org.scalameta" %% "munit" % "1.0.0-M7" % Test,
  "junit" % "junit" % "4.13" % Test,
  "com.github.sbt" % "junit-interface" % "0.13.3" % Test,
  "com.typesafe.akka" %% "akka-actor" % akkaVersion,
  "com.typesafe.akka" %% "akka-testkit" % akkaVersion,
  // SLF4J backend
  // See https://doc.akka.io/docs/akka/current/typed/logging.html#slf4j-backend
  "ch.qos.logback" % "logback-classic" % logbackVersion
)
scalacOptions ++= Seq("-unchecked", "-deprecation")

// Enable debug logging for akka
//javaOptions ++= Seq("-Dakka.loglevel=Debug", "-Dakka.actor.debug.receive=on")

Test / fork := true
Test / parallelExecution := false
Test / testOptions += Tests.Argument(TestFrameworks.JUnit)

enablePlugins(JmhPlugin, ScalafmtPlugin)
