# Demo code for the course CS-206: Parallelism and concurrency

Everybody is welcome to contribute by creating merge requests!

## Installation

1. Check that you have the "Scala (Metals)" and "Scala Syntax (official)" extensions installed in VSCode.
1. Clone this repo: `git clone git@gitlab.epfl.ch:lara/cs206-demos.git`.
1. Open it with VSCode (for example using the command `code .` in `cs206-demos`).
1. Click on "Import SBT Build":
     
     <img src="https://gitlab.epfl.ch/lamp/cs210/uploads/b1926bf55d4d4ce136859afaaad1c7f2/Screenshot_2021-09-30_at_13.22.36.png" width="500px" />

1. Wait while Metals runs `bloopInstal`:
     
     <img src="https://gitlab.epfl.ch/lamp/cs210/uploads/58d513dee61f3584f3e5cf8b10d3510c/Screenshot_2021-09-30_at_13.24.37.png" width="400px" />

## Usage

### Standard Scala files

“Normal” Scala files (ending with `.scala`) must have a `@main` method. For example, [`01-java-threads.scala`](src/main/scala/lecture1/01-javaThreads.scala), the main method is `testThreads`. You can run it from the command-line, first by entering the sbt shell:

```bash
sbt
```
and then by running:

```bash
runMain javaThreads
```

where `javaThreads` is the name of a `@main` function. This should output something similar to:

```
sbt:cs206-demos> run javaThreads
[info] compiling 1 Scala source to /Users/me/cs206-demos/target/scala-3.2.0/classes ...
[info] running javaThreads javaThreads
Little threads did not start yet!
ThreadThread-3 has counter 0
ThreadThread-2 has counter 0
ThreadThread-2 has counter 1
Parent thread and children are running!
...
```

You can also directly run it from VSCode using the `run` link:

![Run link displayed in VSCode](images/run_link.jpg)


### Scala worksheets

Inside `src/main/scala`, you can create Scala _worksheets_ (files ending with `.worksheet.sc`). These are special Scala files meant for experimentation where the result of every top-level expression is directly displayed in the IDE. See [the official tutorial about Scala worksheets](https://docs.scala-lang.org/scala3/book/tools-worksheets.html) for more details.

You can try [`01-sumList.worksheet.sc`](src/main/scala/ex01/01-sumList.worksheet.sc) as an example. If you open it in VSCode, you should see the result of all top-level expressions automatically displayed:

![Example of a worksheet opened in VSCode](images/worksheet_screenshot.jpg)

Try to change the code and you should see the results automatically updating!

### Format code

This project uses [scalafmt](https://scalameta.org/scalafmt/) to format code.

You can run the formatter using `sbt scalafmt`.

The code style is configured in [.scalafmt.conf](.scalafmt.conf).
