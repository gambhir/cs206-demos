package lecture6

import java.util.concurrent.ForkJoinPool

// Run using `running lecture6.ExecutorsCreate`.
@main def ExecutorsCreate =
  val executor = new ForkJoinPool
  executor.execute(new Runnable:
    override def run() = log("This task is run asynchronously.")
  )
  Thread.sleep(500)
