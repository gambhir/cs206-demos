package lecture6

class Fork(var index: Int)

def philosopherTurn(i: Int, l: Fork, r: Fork): Unit =
  Thread.sleep(10)

  var (left, right) =
    if i % 2 == 0 then (l, r)
    else (r, l)
    // if l.index < r.index then (l, r) else (r, l)

  left.synchronized {
    log(f"Philosopher $i picks up left fork ${left.index}")
    right.synchronized {
      log(f"Philosopher $i picks up right fork ${right.index} - eating")
      Thread.sleep(100)
      log(f"Philosopher $i puts down right fork ${right.index}")
    }
    log(f"Philosopher $i puts down left fork ${left.index}")
  }

@main def run() =
  val n = 5
  val k: Int = 3
  val forks = new Array[Fork](n)
  val philosophers = new Array[Thread](n)
  for p <- 0 to n - 1 do
    forks(p) = Fork(p)

  for p <- 0 to n - 1 do
    philosophers(p) = new Thread:
      override def run() =
        for _ <- 0 until k do
          philosopherTurn(p, forks(p % n), forks((p + 1) % n))
    philosophers(p).start

  for p <- 0 to n - 1 do
    philosophers(p).join()
