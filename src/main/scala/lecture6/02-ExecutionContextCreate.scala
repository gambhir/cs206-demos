package lecture6

import scala.concurrent.ExecutionContext
@main def ExecutionContextCreate =
  val ectx = ExecutionContext.global
  ectx.execute(new Runnable:
    override def run() = log("This task is run asynchronously.")
  )
  Thread.sleep(500)
