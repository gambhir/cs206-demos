package benchmarks

import org.openjdk.jmh.annotations.*

@State(Scope.Benchmark)
class SumBenchmark:
  val bigArray: Array[Int] = (1 to 10000).toArray
  val bigList: List[Int] = (1 to 10000).toList

  @Benchmark
  def bench00_arraySumMethod =
    bigArray.sum

  @Benchmark
  def bench01_arrayWhileLoopIndex(): Int =
    val length = bigArray.length
    var i = 0
    var sum = 0
    while i < length do
      sum += bigArray(i)
      i = i + 1
    sum

  @Benchmark
  def bench02_arrayForLoopIndex(): Int =
    var sum = 0
    for i <- 0 until bigArray.length do sum += bigArray(i)
    sum

  final class OptimizedRange(from: Int, to: Int):
    inline def foreach(inline f: Int => Unit): Unit =
      var i = from
      while i < to do
        f(i)
        i = i + 1

  @Benchmark
  def bench03_arrayOptimizedForLoopIndex() =
    var sum = 0
    for i <- OptimizedRange(0, bigArray.length) do sum += bigArray(i)
    sum

  @Benchmark
  def bench04_arrayForLoop() =
    var sum = 0
    for n <- bigArray do sum += n
    sum

  extension (array: Array[Int])
    inline def optimizedForeach(f: Int => Unit): Unit =
      val to = array.length
      var i = 0
      while i < to do
        f(i)
        i = i + 1

  @Benchmark
  def bench05_arrayOptimizedForeach() =
    var sum = 0
    bigArray.optimizedForeach(sum += _)
    sum

  extension (array: Array[Int])
    inline def optimizedForeachArgInlined(inline f: Int => Unit): Unit =
      val to = array.length
      var i = 0
      while i < to do
        f(i)
        i = i + 1

  @Benchmark
  def bench06_arrayOptimizedForeachArgInlined() =
    var sum = 0
    bigArray.optimizedForeach(sum += _)
    sum

  @Benchmark
  def bench07_listSumMethod =
    bigList.sum

  @Benchmark
  def bench08_listForLoopIndex(): Int =
    var sum = 0
    for i <- 0 until bigList.length do sum += bigList(i)
    sum

  @Benchmark
  def bench09_listForLoop() =
    var sum = 0
    for n <- bigList do sum += n
    sum

  extension (list: List[Int])
    inline def optimizedForeach(f: Int => Unit): Unit =
      var curr = list
      while curr != Nil do
        f(curr.head)
        curr = curr.tail

  @Benchmark
  def bench10_listOptimizedForeach() =
    var sum = 0
    bigList.optimizedForeach(sum += _)
    sum

  extension (list: List[Int])
    inline def optimizedForeachArgInlined(inline f: Int => Unit): Unit =
      var curr = list
      while curr != Nil do
        f(curr.head)
        curr = curr.tail

  @Benchmark
  def bench11_listOptimizedForeachArgInlined() =
    var sum = 0
    bigList.optimizedForeach(sum += _)
    sum
