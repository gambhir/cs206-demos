package benchmarks

import org.openjdk.jmh.annotations.*

@State(Scope.Benchmark)
class AppendBenchmark:
  @Param(Array("List", "Array"))
  var seqType: String = _

  var bigSeq: Seq[Int] = _

  @Setup(Level.Trial)
  def setup =
    bigSeq = seqType match
      case "List"  => (1 to 10000).toList
      case "Array" => (1 to 10000).toArray.toIndexedSeq

  @Benchmark
  def bench00_prepend = 0 +: bigSeq

  @Benchmark
  def bench01_append = bigSeq :+ 0
