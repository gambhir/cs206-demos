//    PDF: https://moodle.epfl.ch/pluginfile.php/3175938/mod_folder/content/0/week03-4-Scala-Parallel-Collections.pdf
//  Video: https://mediaspace.epfl.ch/playlist/dedicated/31866/0_icv10qux/0_h4jt9i7k
// Slides: 29

package lecture3

import scala.collection.parallel.CollectionConverters.IterableIsParallelizable
import scala.collection.parallel.immutable.ParSet
import java.util.concurrent.ConcurrentSkipListSet

// Note: we don't use `GenSet` because it is deprecated since Scala 2.13.

def intersectionParNoSideEffect(a: ParSet[Int], b: ParSet[Int]) =
  if a.size < b.size then a.filter(b(_))
  else b.filter(a(_))

@main def intersectionNoSideEffect =
  val a = (0 until 10000).toSet
  val b = (0 until 10000 by 4).toSet
  val seqRes = a.intersect(b)
  val parRes = intersectionParNoSideEffect(a.par.toSet, b.par.toSet)
  assert(seqRes.size == 2500)
  assert(parRes.size == 2500)
