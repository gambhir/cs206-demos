//    PDF: https://moodle.epfl.ch/pluginfile.php/3175938/mod_folder/content/0/week01-5-First-Class-Tasks.pdf
//  Video: https://mediaspace.epfl.ch/playlist/dedicated/31866/0_c46icdbx/0_v7c766xv
// Slides: 7

package lecture1

import lecture1.{task, Task}
def parallel[A, B](taskA: => A, taskB: => B): (A, B) =
  val tB: Task[B] = task {
    taskB
  }
  val tA: A = taskA
  (tA, tB.join)
