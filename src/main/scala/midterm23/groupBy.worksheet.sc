import lecture1.parallel
import collection.concurrent.TrieMap
import collection.parallel.CollectionConverters.*

// Example run of `groupBy`:
case class Course(semester: String, id: String)
val cs210 = Course("BA3", "CS210")
val cs250 = Course("BA3", "CS250")
val cs206 = Course("BA4", "CS206")
val courses = Vector(cs210, cs250, cs206)
assertEquals(
  courses.groupBy(_.semester),
  Map(
    "BA3" -> Vector(cs210, cs250),
    "BA4" -> Vector(cs206)
  )
)

/** Asymptotic run time: O(1) */
def appendAtKey[K, V](m: Map[K, Vector[V]], k: K, v: V): Map[K, Vector[V]] =
  val prev = m.getOrElse(k, Vector.empty)
  m.updated(k, prev :+ v)

// Example runs of `appendAtKey`:
assertEquals(
  appendAtKey(Map(), 0, 1),
  Map(0 -> Vector(1))
)
assertEquals(
  appendAtKey(Map("BA3" -> Vector(cs210)), "BA3", cs250),
  Map("BA3" -> List(cs210, cs250))
)

/** Asymptotic run time: O(1) */
def mergeMaps[K, V](a: Map[K, Vector[V]], b: Map[K, Vector[V]]) =
  var res = a
  for (k, vs) <- b do // O(1) iterations (we assume the number of keys to be O(1))
    val prev = res.getOrElse(k, Vector.empty) // O(1) lookup
    res = res.updated(k, prev ++ vs) // O(1) update, O(1) concatenation
  res

// Example run of `mergeMaps`:
val m1 = Map(1 -> Vector(5, 7))
val m2 = Map(1 -> Vector(6), 2 -> Vector(3))
assertEquals(
  mergeMaps(m1, m2),
  Map(
    1 -> Vector(5, 7, 6),
    2 -> Vector(3)
  )
)

def parGroupBy1[K, V](vs: Vector[V], key: V => K): Map[K, Vector[V]] =
  if vs.size == 1 then
    Map(key(vs.head) -> Vector(vs.head))
  else
    val mid = vs.size / 2
    val (m1, m2) = parallel(
      parGroupBy1(vs.slice(0, mid), key),
      parGroupBy1(vs.slice(mid, vs.size), key)
    )
    mergeMaps(m1, m2)

assertEquals(
  parGroupBy1(courses, c => c.semester),
  Map(
    "BA3" -> List(cs210, cs250),
    "BA4" -> List(cs206)
  )
)

assertEquals(
  parGroupBy1(courses, c => c.semester),
  Map(
    "BA3" -> List(cs210, cs250),
    "BA4" -> List(cs206)
  )
)

def parGroupBy2[K, V](vs: Vector[V], key: V => K): Map[K, Vector[V]] =
  if vs.size == 1 then
    Map(key(vs.head) -> Vector(vs.head))
  else
    val (m1, m2) = parallel(
      parGroupBy2(Vector(vs.head), key),
      parGroupBy2(vs.tail, key)
    )
    mergeMaps(m1, m2)

assertEquals(
  parGroupBy2(courses, c => c.semester),
  Map(
    "BA3" -> List(cs210, cs250),
    "BA4" -> List(cs206)
  )
)

assertEquals(
  parGroupBy2(courses, c => c.semester),
  Map(
    "BA3" -> List(cs210, cs250),
    "BA4" -> List(cs206)
  )
)

def parGroupBy3[K, V](vs: Vector[V], key: V => K): Map[K, Vector[V]] =
  val lock = Object()
  var res = Map[K, Vector[V]]()
  for v <- vs.par do
    lock.synchronized { res = appendAtKey(res, key(v), v) }
  res

var values3 = Set[Map[String, Vector[Course]]]()
for _ <- 1 to 100000 do
  values3 += parGroupBy3(courses, _.semester)
assertEquals(values3.size, 2)
values3

def parGroupBy4[K, V](vs: Vector[V], key: V => K): Map[K, Vector[V]] =
  val res = TrieMap[K, Vector[V]]()
  for v <- vs.par do
    val k = key(v)
    val prev = res.getOrElse(k, Vector.empty)
    res.update(k, prev :+ v)
  res.toMap

var values4 = Set[Map[String, Vector[Course]]]()
for _ <- 1 to 100000 do
  values4 += parGroupBy4(courses, _.semester)
assertEquals(values4.size, 4)
values4

def parGroupBySolution[K, V](vs: Vector[V], key: V => K): Map[K, Vector[V]] =
  vs.par.aggregate(Map.empty)(
    (m, v) => appendAtKey(m, key(v), v),
    mergeMaps
  )

var valuesSol = Set[Map[String, Vector[Course]]]()
for _ <- 1 to 100000 do
  valuesSol += parGroupBySolution(courses, _.semester)
assertEquals(valuesSol.size, 1)
valuesSol

def assertEquals(a: Any, b: Any) = assert(a == b)
