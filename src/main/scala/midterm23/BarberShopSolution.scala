package midterm23

import instrumentation.Monitor
import scala.annotation.tailrec

/** Runs one of the three provided solutions to the barber shop problem.
  *
  * Run using `sbt "runMain midterm23.barberSolution 2 3"` for example.
  *
  * @param solutionNumber
  *   One of the 3 provided solutions to run: 1, 2, or 3.
  * @param nCustomers
  *   The number of customers to simulate.
  */
@main def barberSolution(solutionNumber: Int, nCustomers: Int): Unit =
  val barberShop: AbstractBarberShopSolution =
    solutionNumber match
      case 1 => BarberShopSolution1(nCustomers)
      case 2 => BarberShopSolution2(nCustomers)
      case 3 => BarberShopSolution3(nCustomers)

  barberSolutionMain(barberShop, solutionNumber, nCustomers)

def barberSolutionMain(
    barberShop: AbstractBarberShopSolution,
    solutionNumber: Int,
    nCustomers: Int
): Unit =
  val bthread = new Thread:
    override def run() =
      barberShop.barber()
  bthread.start()

  val customerThreads =
    for i <- 1 to nCustomers yield
      val cthread = new Thread:
        override def run() =
          barberShop.customer(i)
      cthread.start()
      cthread

  bthread.join()
  customerThreads.foreach(_.join())

abstract class AbstractBarberShopSolution:
  // var notifyBarber: Boolean = false
  // var notifyCustomer: Boolean = false
  // val lockObj = Object()

  // In order to be able to mock the three attributes above in the tests, we
  // replace them with three private attributes and public getters and setters.
  // Without overrides, this is equivalent to the definitions above.

  private var _notifyBarber: Boolean = false
  private var _notifyCustomer: Boolean = false
  private val _lockObject: Monitor = new Monitor {}
  def notifyBarber = _notifyBarber
  def notifyBarber_=(v: Boolean) = _notifyBarber = v
  def notifyCustomer = _notifyCustomer
  def notifyCustomer_=(v: Boolean) = _notifyCustomer = v
  def lockObj: Monitor = _lockObject

  def hairCut(): Unit =
    log("start hair cut")
    Thread.sleep(1000)
    log("finish hair cut")

  def customer(n: Int): Unit
  def barber(): Unit

  /** Logs a message to the console.
    *
    * Overridden in the tests to avoid printing gigabytes of output.
    *
    * @param s
    */
  def log(s: String) = println(s)

/** This is the solution we expected for the `customer` method.
  *
  * However, now that you have learned about `@volatile`, you can see that the
  * `customer` method is not thread-safe. Can you spot a problem with this
  * solution?
  *
  * Note that the `log` calls were added to help you understand what is going
  * on. They are not part of the expected solution.
  */
trait BarberShopCustomerSolution1 extends AbstractBarberShopSolution:
  override def customer(n: Int) =
    lockObj.synchronized {
      log(f"customer ${n} sits on the chair and waits for the barber")
      notifyBarber = true
      while !notifyCustomer do {}
      notifyCustomer = false
      log(f"customer ${n} leaves the chair")
    }

/** This is the solution we expected for the `barber` method.
  *
  * Same question as above: can you spot a problem with this solution?
  *
  * @param customersNumber
  *   The number of customers to simulate.
  */
trait BarberShopBarberSolution1(customersNumber: Int)
    extends AbstractBarberShopSolution:
  override def barber() =
    // while true do

    // We replace the infinite loop from the instructions with a loop doing a
    // limited number of iterations so that we can test the code.
    for _ <- 1 to customersNumber do
      log("barber waits for a customer")
      while !notifyBarber do {}
      notifyBarber = false
      hairCut()
      notifyCustomer = true

    log("barber stops working")

class BarberShopSolution1(customersNumber: Int)
    extends BarberShopCustomerSolution1,
      BarberShopBarberSolution1(customersNumber)

/** The problem with BarberShopSolution1 is that the `notifyBarber` and
  * `notifyCustomer` are accessed from multiple threads without any
  * synchronization. This means that the compiler is free to reorder the
  * instructions in the `customer` and `barber` methods, which can lead to
  * unexpected behaviors like infinite loops!
  *
  * To solve this problem, the solution below redefines the two attributes as
  * `@volatile`.
  *
  * @param customersNumber
  *   The number of customers to simulate.
  */
class BarberShopSolution2(customersNumber: Int)
    extends BarberShopSolution1(customersNumber):
  @volatile private var _notifyBarber: Boolean = false
  @volatile private var _notifyCustomer: Boolean = false

  override def notifyBarber = _notifyBarber
  override def notifyBarber_=(v: Boolean) = _notifyBarber = v
  override def notifyCustomer = _notifyCustomer
  override def notifyCustomer_=(v: Boolean) = _notifyCustomer = v

/** The solution below uses a different approach to solve the problem of
  * BarberShopSolution1. Instead of using `@volatile`, it uses the lock object
  * to synchronize the accesses to the two attributes. Note how each access to
  * the attributes is now wrapped in a `lockObj.synchronized` block.
  *
  * @param customersNumber
  *   The number of customers to simulate.
  */
class BarberShopSolution3(customersNumber: Int)
    extends AbstractBarberShopSolution:
  override def customer(n: Int) =
    getHairCut(n)

  @tailrec
  private def getHairCut(n: Int): Unit =
    val sited = lockObj.synchronized {
      if notifyBarber then
        false
      else if notifyCustomer then
        false
      else
        log(f"customer ${n} sits on the chair and waits for the barber")
        notifyBarber = true
        true
    }
    if sited then
      while !lockObj.synchronized { notifyCustomer } do {}
      lockObj.synchronized {
        log(f"customer ${n} leaves the chair")
        notifyCustomer = false
      }
    else
      getHairCut(n)

  override def barber() =
    for _ <- 1 to customersNumber do
      log("barber waits for a customer")

      while !lockObj.synchronized { notifyBarber } do {}
      hairCut()
      lockObj.synchronized {
        notifyCustomer = true
        notifyBarber = false
      }

    log("barber stops working")
