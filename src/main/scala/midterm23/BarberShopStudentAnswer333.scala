package midterm23

import java.util.concurrent.atomic.AtomicInteger

/** This is an example `BarberShopStudentAnswer`, like the one we made for your
  * own solution and that you can find on Moodle. The solution below is boringly
  * correct as it use the same implementation as the expected solution. See
  * BarberShopSolution.scala for explanations and comments.
  *
  * @param customersNumber
  *   The number of customers to simulate.
  */
class BarberShopStudentAnswer333(customersNumber: Int)
    extends AbstractBarberShopSolution:

  /** This attribute is used to count the number of hair cuts. It is needed for
    * testing because we do not want to loop forever in the `barber` method!
    */
  val hairCutsCounter = AtomicInteger(customersNumber)

  override def customer(n: Int) =
    lockObj.synchronized {
      notifyBarber = true
      while !notifyCustomer do {}
      notifyCustomer = false
    }

  override def barber() =
    // while true

    // We replace the infinite loop from the instructions with a loop doing a
    // limited number of iterations so that we can test the code.
    while hairCutsCounter.get() != 0 do
      while !notifyBarber do {}
      notifyBarber = false
      hairCut()
      hairCutsCounter.decrementAndGet() // You can ignore this line. It has been added for testing.
      notifyCustomer = true

/** Version of `BarberShopStudentAnswer333` with the methods `customer`
  * overridden to use the expected implementation.
  *
  * @param n
  *   The number of customers to simulate.
  */
class BarberShopStudentAnswer333WithCorrectCustomer(n: Int)
    extends BarberShopStudentAnswer333(n)
    with BarberShopCustomerSolution1

/** Version of `BarberShopStudentAnswer333` with the methods `barber` overridden
  * to use the expected implementation.
  *
  * @param n
  *   The number of customers to simulate.
  */
class BarberShopStudentAnswer333WithCorrectBarber(n: Int)
    extends BarberShopStudentAnswer333(n)
    with BarberShopBarberSolution1(n)

// To grade, we ran tests with each of these classes.
