package instrumentation

import java.lang.management.*

/** A collection of methods that can be used to collect run-time statistics */
object Stats:
  def timed[T](code: => T)(cont: Long => Unit): T =
    var t1 = System.currentTimeMillis()
    val r = code
    cont((System.currentTimeMillis() - t1))
    r

  def withTime[T](code: => T): (T, Long) =
    var t1 = System.currentTimeMillis()
    val r = code
    (r, (System.currentTimeMillis() - t1))
