package midterm23

import instrumentation.{Scheduler, TestHelper}

/** Tests a barber shop implementation.
  *
  * @param nCustomers
  *   The number of customers to simulate.
  * @param makeShop
  *   A function that creates a barber shop given the number of customers and a
  *   scheduler.
  * @param checkEarlyCustomer
  *   If true, checks that no customer left early, i.e. that there is always a
  *   number of terminated customer threads equal or less than to the number of
  *   haircuts done.
  */
def testBarberShop(
    nCustomers: Int,
    makeShop: (Int, Scheduler) => ScheduledBarberShopSolution,
    checkEarlyCustomer: Boolean = true
) =
  TestHelper.testManySchedules(
    nCustomers + 1,
    scheduler =>
      val barberShop = makeShop(nCustomers, scheduler)

      (
        (() => barberShop.barber())
          :: (for i <- 1 to nCustomers
          yield () => barberShop.customerTrace(i)).toList,
        results =>
          if barberShop.nHaircuts != nCustomers then
            (false, f"Unexpected number of hair cuts: ${barberShop.nHaircuts}")
          else if checkEarlyCustomer && barberShop.customerLeftEarly() then
            (false, f"A customer left early")
          else
            (true, "")
      )
  )
