[info] welcome to sbt 1.7.1 (GraalVM Community Java 17.0.6)
[info] loading settings for project cs206-demos-build-build-build from metals.sbt ...
[info] loading project definition from /Users/mbovel/cs206-demos/project/project/project
[info] loading settings for project cs206-demos-build-build from metals.sbt ...
[info] loading project definition from /Users/mbovel/cs206-demos/project/project
[success] Generated .bloop/cs206-demos-build-build.json
[success] Total time: 0 s, completed Mar 5, 2023, 12:50:13 PM
[info] loading settings for project cs206-demos-build from metals.sbt,plugins.sbt ...
[info] loading project definition from /Users/mbovel/cs206-demos/project
[success] Generated .bloop/cs206-demos-build.json
[success] Total time: 0 s, completed Mar 5, 2023, 12:50:14 PM
[info] loading settings for project cs206-demos from build.sbt ...
[info] set current project to cs206-demos (in build file:/Users/mbovel/cs206-demos/)
[success] Total time: 0 s, completed Mar 5, 2023, 12:50:15 PM
[info] compiling 7 Scala sources and 1 Java source to /Users/mbovel/cs206-demos/target/scala-3.2.0/classes ...
[warn] there was 1 deprecation warning; re-run with -deprecation for details
[warn] one warning found
[info] done compiling
[info] running org.openjdk.jmh.generators.bytecode.JmhBytecodeGenerator /Users/mbovel/cs206-demos/target/scala-3.2.0/classes /Users/mbovel/cs206-demos/target/scala-3.2.0/src_managed/jmh /Users/mbovel/cs206-demos/target/scala-3.2.0/resource_managed/jmh default
Processing 18 classes from /Users/mbovel/cs206-demos/target/scala-3.2.0/classes with "reflection" generator
Writing out Java source to /Users/mbovel/cs206-demos/target/scala-3.2.0/src_managed/jmh and resources to /Users/mbovel/cs206-demos/target/scala-3.2.0/resource_managed/jmh
[info] compiling 22 Java sources to /Users/mbovel/cs206-demos/target/scala-3.2.0/classes ...
[info] done compiling
[info] running (fork) org.openjdk.jmh.Main -wi 5 -w 2 -i 5 -r 2 -f 4 -rf JSON -rff benchmarks/results/01-AppendBenchmark-graal-2.json AppendBenchmark
[info] # JMH version: 1.36
[info] # VM version: JDK 17.0.6, OpenJDK 64-Bit Server VM, 17.0.6+10-jvmci-22.3-b13
[info] # VM invoker: /private/var/root/Library/Caches/Coursier/arc/https/github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.3.1/graalvm-ce-java17-darwin-amd64-22.3.1.tar.gz/graalvm-ce-java17-22.3.1/Contents/Home/bin/java
[info] # VM options: -XX:ThreadPriorityPolicy=1 -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCIProduct -XX:JVMCIThreadsPerNativeLibraryRuntime=1 -XX:-UnlockExperimentalVMOptions
[info] # Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
[info] # Warmup: 5 iterations, 2 s each
[info] # Measurement: 5 iterations, 2 s each
[info] # Timeout: 10 min per iteration
[info] # Threads: 1 thread, will synchronize iterations
[info] # Benchmark mode: Throughput, ops/time
[info] # Benchmark: benchmarks.AppendBenchmark.bench00_prepend
[info] # Parameters: (seqType = List)
[info] # Run progress: 0.00% complete, ETA 00:05:20
[info] # Fork: 1 of 4
[info] # Warmup Iteration   1: 391613636.238 ops/s
[info] # Warmup Iteration   2: 429588721.966 ops/s
[info] # Warmup Iteration   3: 428049250.909 ops/s
[info] # Warmup Iteration   4: 406963029.136 ops/s
[info] # Warmup Iteration   5: 401672158.701 ops/s
[info] Iteration   1: 406584848.191 ops/s
[info] Iteration   2: 399782141.245 ops/s
[info] Iteration   3: 356308889.512 ops/s
[info] Iteration   4: 368230572.178 ops/s
[info] Iteration   5: 403090721.600 ops/s
[info] # Run progress: 6.25% complete, ETA 00:05:05
[info] # Fork: 2 of 4
[info] # Warmup Iteration   1: 364466258.024 ops/s
[info] # Warmup Iteration   2: 415278299.041 ops/s
[info] # Warmup Iteration   3: 418841262.042 ops/s
[info] # Warmup Iteration   4: 416924569.493 ops/s
[info] # Warmup Iteration   5: 421497807.735 ops/s
[info] Iteration   1: 423989078.657 ops/s
[info] Iteration   2: 418625612.168 ops/s
[info] Iteration   3: 423633328.418 ops/s
[info] Iteration   4: 421001901.827 ops/s
[info] Iteration   5: 426811951.223 ops/s
[info] # Run progress: 12.50% complete, ETA 00:04:45
[info] # Fork: 3 of 4
[info] # Warmup Iteration   1: 370583021.187 ops/s
[info] # Warmup Iteration   2: 435513606.208 ops/s
[info] # Warmup Iteration   3: 449285411.610 ops/s
[info] # Warmup Iteration   4: 442676172.017 ops/s
[info] # Warmup Iteration   5: 459864180.424 ops/s
[info] Iteration   1: 460082973.502 ops/s
[info] Iteration   2: 457622181.215 ops/s
[info] Iteration   3: 457976522.076 ops/s
[info] Iteration   4: 447162800.425 ops/s
[info] Iteration   5: 458168575.371 ops/s
[info] # Run progress: 18.75% complete, ETA 00:04:25
[info] # Fork: 4 of 4
[info] # Warmup Iteration   1: 401558978.752 ops/s
[info] # Warmup Iteration   2: 429946047.540 ops/s
[info] # Warmup Iteration   3: 452599874.717 ops/s
[info] # Warmup Iteration   4: 455637485.376 ops/s
[info] # Warmup Iteration   5: 461309972.898 ops/s
[info] Iteration   1: 460820994.973 ops/s
[info] Iteration   2: 456153244.979 ops/s
[info] Iteration   3: 463252295.492 ops/s
[info] Iteration   4: 451026908.849 ops/s
[info] Iteration   5: 445879204.724 ops/s
[info] Result "benchmarks.AppendBenchmark.bench00_prepend":
[info]   430310237.331 ±(99.9%) 27241588.295 ops/s [Average]
[info]   (min, avg, max) = (356308889.512, 430310237.331, 463252295.492), stdev = 31371453.592
[info]   CI (99.9%): [403068649.037, 457551825.626] (assumes normal distribution)
[info] # JMH version: 1.36
[info] # VM version: JDK 17.0.6, OpenJDK 64-Bit Server VM, 17.0.6+10-jvmci-22.3-b13
[info] # VM invoker: /private/var/root/Library/Caches/Coursier/arc/https/github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.3.1/graalvm-ce-java17-darwin-amd64-22.3.1.tar.gz/graalvm-ce-java17-22.3.1/Contents/Home/bin/java
[info] # VM options: -XX:ThreadPriorityPolicy=1 -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCIProduct -XX:JVMCIThreadsPerNativeLibraryRuntime=1 -XX:-UnlockExperimentalVMOptions
[info] # Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
[info] # Warmup: 5 iterations, 2 s each
[info] # Measurement: 5 iterations, 2 s each
[info] # Timeout: 10 min per iteration
[info] # Threads: 1 thread, will synchronize iterations
[info] # Benchmark mode: Throughput, ops/time
[info] # Benchmark: benchmarks.AppendBenchmark.bench00_prepend
[info] # Parameters: (seqType = Array)
[info] # Run progress: 25.00% complete, ETA 00:04:04
[info] # Fork: 1 of 4
[info] # Warmup Iteration   1: 300145.909 ops/s
[info] # Warmup Iteration   2: 366703.342 ops/s
[info] # Warmup Iteration   3: 366284.098 ops/s
[info] # Warmup Iteration   4: 360692.130 ops/s
[info] # Warmup Iteration   5: 362061.133 ops/s
[info] Iteration   1: 368471.311 ops/s
[info] Iteration   2: 364389.493 ops/s
[info] Iteration   3: 369274.428 ops/s
[info] Iteration   4: 361768.574 ops/s
[info] Iteration   5: 369455.746 ops/s
[info] # Run progress: 31.25% complete, ETA 00:03:44
[info] # Fork: 2 of 4
[info] # Warmup Iteration   1: 289697.373 ops/s
[info] # Warmup Iteration   2: 370188.440 ops/s
[info] # Warmup Iteration   3: 372434.071 ops/s
[info] # Warmup Iteration   4: 363520.485 ops/s
[info] # Warmup Iteration   5: 372521.822 ops/s
[info] Iteration   1: 371790.930 ops/s
[info] Iteration   2: 373053.102 ops/s
[info] Iteration   3: 367582.530 ops/s
[info] Iteration   4: 362853.883 ops/s
[info] Iteration   5: 371606.448 ops/s
[info] # Run progress: 37.50% complete, ETA 00:03:23
[info] # Fork: 3 of 4
[info] # Warmup Iteration   1: 289813.800 ops/s
[info] # Warmup Iteration   2: 369458.667 ops/s
[info] # Warmup Iteration   3: 372437.739 ops/s
[info] # Warmup Iteration   4: 364467.158 ops/s
[info] # Warmup Iteration   5: 371778.750 ops/s
[info] Iteration   1: 328081.372 ops/s
[info] Iteration   2: 369703.497 ops/s
[info] Iteration   3: 371471.684 ops/s
[info] Iteration   4: 362630.111 ops/s
[info] Iteration   5: 372868.500 ops/s
[info] # Run progress: 43.75% complete, ETA 00:03:03
[info] # Fork: 4 of 4
[info] # Warmup Iteration   1: 289966.934 ops/s
[info] # Warmup Iteration   2: 369864.795 ops/s
[info] # Warmup Iteration   3: 370581.719 ops/s
[info] # Warmup Iteration   4: 318985.504 ops/s
[info] # Warmup Iteration   5: 287819.941 ops/s
[info] Iteration   1: 310889.685 ops/s
[info] Iteration   2: 313411.212 ops/s
[info] Iteration   3: 299374.767 ops/s
[info] Iteration   4: 300812.244 ops/s
[info] Iteration   5: 305217.473 ops/s
[info] Result "benchmarks.AppendBenchmark.bench00_prepend":
[info]   350735.349 ±(99.9%) 24594.574 ops/s [Average]
[info]   (min, avg, max) = (299374.767, 350735.349, 373053.102), stdev = 28323.148
[info]   CI (99.9%): [326140.776, 375329.923] (assumes normal distribution)
[info] # JMH version: 1.36
[info] # VM version: JDK 17.0.6, OpenJDK 64-Bit Server VM, 17.0.6+10-jvmci-22.3-b13
[info] # VM invoker: /private/var/root/Library/Caches/Coursier/arc/https/github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.3.1/graalvm-ce-java17-darwin-amd64-22.3.1.tar.gz/graalvm-ce-java17-22.3.1/Contents/Home/bin/java
[info] # VM options: -XX:ThreadPriorityPolicy=1 -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCIProduct -XX:JVMCIThreadsPerNativeLibraryRuntime=1 -XX:-UnlockExperimentalVMOptions
[info] # Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
[info] # Warmup: 5 iterations, 2 s each
[info] # Measurement: 5 iterations, 2 s each
[info] # Timeout: 10 min per iteration
[info] # Threads: 1 thread, will synchronize iterations
[info] # Benchmark mode: Throughput, ops/time
[info] # Benchmark: benchmarks.AppendBenchmark.bench01_append
[info] # Parameters: (seqType = List)
[info] # Run progress: 50.00% complete, ETA 00:02:43
[info] # Fork: 1 of 4
[info] # Warmup Iteration   1: 24454.779 ops/s
[info] # Warmup Iteration   2: 27701.177 ops/s
[info] # Warmup Iteration   3: 29648.150 ops/s
[info] # Warmup Iteration   4: 29607.508 ops/s
[info] # Warmup Iteration   5: 30850.342 ops/s
[info] Iteration   1: 30280.075 ops/s
[info] Iteration   2: 30099.647 ops/s
[info] Iteration   3: 30000.580 ops/s
[info] Iteration   4: 30430.947 ops/s
[info] Iteration   5: 30606.989 ops/s
[info] # Run progress: 56.25% complete, ETA 00:02:22
[info] # Fork: 2 of 4
[info] # Warmup Iteration   1: 24748.018 ops/s
[info] # Warmup Iteration   2: 28221.599 ops/s
[info] # Warmup Iteration   3: 30973.738 ops/s
[info] # Warmup Iteration   4: 31247.369 ops/s
[info] # Warmup Iteration   5: 30812.258 ops/s
[info] Iteration   1: 31222.031 ops/s
[info] Iteration   2: 31107.874 ops/s
[info] Iteration   3: 30658.834 ops/s
[info] Iteration   4: 31381.319 ops/s
[info] Iteration   5: 31282.972 ops/s
[info] # Run progress: 62.50% complete, ETA 00:02:02
[info] # Fork: 3 of 4
[info] # Warmup Iteration   1: 25681.439 ops/s
[info] # Warmup Iteration   2: 28214.213 ops/s
[info] # Warmup Iteration   3: 30657.366 ops/s
[info] # Warmup Iteration   4: 30526.860 ops/s
[info] # Warmup Iteration   5: 31030.973 ops/s
[info] Iteration   1: 31221.948 ops/s
[info] Iteration   2: 31305.710 ops/s
[info] Iteration   3: 30892.019 ops/s
[info] Iteration   4: 31210.856 ops/s
[info] Iteration   5: 31854.893 ops/s
[info] # Run progress: 68.75% complete, ETA 00:01:41
[info] # Fork: 4 of 4
[info] # Warmup Iteration   1: 24203.958 ops/s
[info] # Warmup Iteration   2: 28595.311 ops/s
[info] # Warmup Iteration   3: 30283.665 ops/s
[info] # Warmup Iteration   4: 30848.345 ops/s
[info] # Warmup Iteration   5: 30782.946 ops/s
[info] Iteration   1: 31410.115 ops/s
[info] Iteration   2: 30272.118 ops/s
[info] Iteration   3: 30053.846 ops/s
[info] Iteration   4: 31355.272 ops/s
[info] Iteration   5: 30208.880 ops/s
[info] Result "benchmarks.AppendBenchmark.bench01_append":
[info]   30842.846 ±(99.9%) 487.563 ops/s [Average]
[info]   (min, avg, max) = (30000.580, 30842.846, 31854.893), stdev = 561.478
[info]   CI (99.9%): [30355.283, 31330.409] (assumes normal distribution)
[info] # JMH version: 1.36
[info] # VM version: JDK 17.0.6, OpenJDK 64-Bit Server VM, 17.0.6+10-jvmci-22.3-b13
[info] # VM invoker: /private/var/root/Library/Caches/Coursier/arc/https/github.com/graalvm/graalvm-ce-builds/releases/download/vm-22.3.1/graalvm-ce-java17-darwin-amd64-22.3.1.tar.gz/graalvm-ce-java17-22.3.1/Contents/Home/bin/java
[info] # VM options: -XX:ThreadPriorityPolicy=1 -XX:+UnlockExperimentalVMOptions -XX:+EnableJVMCIProduct -XX:JVMCIThreadsPerNativeLibraryRuntime=1 -XX:-UnlockExperimentalVMOptions
[info] # Blackhole mode: compiler (auto-detected, use -Djmh.blackhole.autoDetect=false to disable)
[info] # Warmup: 5 iterations, 2 s each
[info] # Measurement: 5 iterations, 2 s each
[info] # Timeout: 10 min per iteration
[info] # Threads: 1 thread, will synchronize iterations
[info] # Benchmark mode: Throughput, ops/time
[info] # Benchmark: benchmarks.AppendBenchmark.bench01_append
[info] # Parameters: (seqType = Array)
[info] # Run progress: 75.00% complete, ETA 00:01:21
[info] # Fork: 1 of 4
[info] # Warmup Iteration   1: 296708.697 ops/s
[info] # Warmup Iteration   2: 369165.270 ops/s
[info] # Warmup Iteration   3: 361730.934 ops/s
[info] # Warmup Iteration   4: 366632.239 ops/s
[info] # Warmup Iteration   5: 369881.138 ops/s
[info] Iteration   1: 370730.842 ops/s
[info] Iteration   2: 370632.890 ops/s
[info] Iteration   3: 363285.066 ops/s
[info] Iteration   4: 372244.152 ops/s
[info] Iteration   5: 370680.470 ops/s
[info] # Run progress: 81.25% complete, ETA 00:01:01
[info] # Fork: 2 of 4
[info] # Warmup Iteration   1: 291605.201 ops/s
[info] # Warmup Iteration   2: 368387.971 ops/s
[info] # Warmup Iteration   3: 365050.658 ops/s
[info] # Warmup Iteration   4: 370492.700 ops/s
[info] # Warmup Iteration   5: 371229.690 ops/s
[info] Iteration   1: 372612.797 ops/s
[info] Iteration   2: 371902.134 ops/s
[info] Iteration   3: 364470.398 ops/s
[info] Iteration   4: 372498.651 ops/s
[info] Iteration   5: 372513.308 ops/s
[info] # Run progress: 87.50% complete, ETA 00:00:40
[info] # Fork: 3 of 4
[info] # Warmup Iteration   1: 290894.283 ops/s
[info] # Warmup Iteration   2: 370728.398 ops/s
[info] # Warmup Iteration   3: 368119.242 ops/s
[info] # Warmup Iteration   4: 374613.175 ops/s
[info] # Warmup Iteration   5: 373738.733 ops/s
[info] Iteration   1: 370391.138 ops/s
[info] Iteration   2: 373237.159 ops/s
[info] Iteration   3: 367240.006 ops/s
[info] Iteration   4: 375421.573 ops/s
[info] Iteration   5: 371487.346 ops/s
[info] # Run progress: 93.75% complete, ETA 00:00:20
[info] # Fork: 4 of 4
[info] # Warmup Iteration   1: 270634.693 ops/s
[info] # Warmup Iteration   2: 365066.628 ops/s
[info] # Warmup Iteration   3: 364739.445 ops/s
[info] # Warmup Iteration   4: 373922.797 ops/s
[info] # Warmup Iteration   5: 371789.586 ops/s
[info] Iteration   1: 357838.105 ops/s
[info] Iteration   2: 340189.382 ops/s
[info] Iteration   3: 329223.051 ops/s
[info] Iteration   4: 337319.078 ops/s
[info] Iteration   5: 370994.328 ops/s
[info] Result "benchmarks.AppendBenchmark.bench01_append":
[info]   364745.594 ±(99.9%) 11566.733 ops/s [Average]
[info]   (min, avg, max) = (329223.051, 364745.594, 375421.573), stdev = 13320.267
[info]   CI (99.9%): [353178.861, 376312.327] (assumes normal distribution)
[info] # Run complete. Total time: 00:05:26
[info] REMEMBER: The numbers below are just data. To gain reusable insights, you need to follow up on
[info] why the numbers are the way they are. Use profilers (see -prof, -lprof), design factorial
[info] experiments, perform baseline and negative tests that provide experimental control, make sure
[info] the benchmarking environment is safe on JVM/OS/HW level, ask for reviews from the domain experts.
[info] Do not assume the numbers tell you what you want them to tell.
[info] NOTE: Current JVM experimentally supports Compiler Blackholes, and they are in use. Please exercise
[info] extra caution when trusting the results, look into the generated code to check the benchmark still
[info] works, and factor in a small probability of new VM bugs. Additionally, while comparisons between
[info] different JVMs are already problematic, the performance difference caused by different Blackhole
[info] modes can be very significant. Please make sure you use the consistent Blackhole mode for comparisons.
[info] Benchmark                        (seqType)   Mode  Cnt          Score          Error  Units
[info] AppendBenchmark.bench00_prepend       List  thrpt   20  430310237.331 ± 27241588.295  ops/s
[info] AppendBenchmark.bench00_prepend      Array  thrpt   20     350735.349 ±    24594.574  ops/s
[info] AppendBenchmark.bench01_append        List  thrpt   20      30842.846 ±      487.563  ops/s
[info] AppendBenchmark.bench01_append       Array  thrpt   20     364745.594 ±    11566.733  ops/s
[info] Benchmark result is saved to benchmarks/results/01-AppendBenchmark-graal-2.json
[success] Total time: 332 s (05:32), completed Mar 5, 2023, 12:55:47 PM
